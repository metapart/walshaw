SOURCES:= $(wildcard *.bz2)
# TARGETS:=$(patsubst %.bz2,%,$(SOURCES))
TARGETS:= $(SOURCES:.graph.bz2=*.graph)

%.graph: %.graph.bz2
	@bunzip2 -v -k -f $<

ALL: $(TARGETS) 

.PHONY: clean

clean:
	@rm -f $(TARGETS)
